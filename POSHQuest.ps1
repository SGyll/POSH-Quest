﻿$console = $host.UI.RawUI

$console.foregroundColor = "green"
$console.backgroundColor = "black"

$size = $console.WindowSize
$size.Width = 130
$size.Height = 50
$console.WindowSize = $size

#Intro

$borkedAtDoor = 0

function startGame{
Write-Host "Welcome to POSH Quest!"

"`n"

cls

Write-Host "`nIt's a cool, sunny morning. You're sleeping on the bed as usual, when a 
scary noise catches your attention. You sit up, startled, as your ears prick to the noise."

firstChoice
}

function firstChoice{
Write-Host "`nWhat do you do?`n
1. Investigate the noise
2. Bork
3. Ignore it and go back to sleep`n"

$choice = Read-host "Enter your choice (1,2 or 3)"

switch ($choice){
    1
    {
        Write-Host "`nDespite the fear that constricts your soul, and the fatigue that constricts your body, you push yourself to your feet and hop down off the bed.`nJourneying into the hallway, you can go left to the living room or right to your friends' room."
        secondChoice
    }

    2
    {
        Write-Host "`nSummoning all the will of your little heart, you bork at the top of your little triangle mouth. Despite your valiant efforts, you hear the sound again momentarily.`n"
        firstChoice
    }

    3
    {
        Write-Host "`nYou lay your head back down on the bed, and close your eyes. Eventually, you fall into a sweet, restful sleep."
        gameOver
    }
    default
    {
        Write-Host "`nNot recognized. Please try again."
        firstChoice
    }
}

}

function secondChoice{

Write-Host "`nWhat will you do?
1. Go left into the living room
2. Go right to your friends' room"

$choice = Read-Host "`nEnter your choice (1 or 2)"

switch ($choice){
    1
    {
        Write-Host "`nYou journey through the living room."
        livingRoom
    }

    2
    {
        Write-Host "`nYou go right, to your friend's room. The door is closed and your mind cannot comprehend how to open it."
        friendsRoomDoor
    }

    default
    {
        Write-Host "`nNot recognized. Please try again."
        firstChoice
    }
}
}

function friendsRoomDoor{
Write-Host "`nWhat will you do?
1. Leave
2. Bork at the door"

$choice = Read-Host "`nEnter your choice (1 or 2)"

switch ($choice)
    {
        1
        {Write-Host "`nDisappointed, you leave the room and go back into the hallway."
         roomOrLiving}

        2
        {Write-Host "`nYou bork with all your might, but other than some faint voices beyond the door, nothing happens."
         $borkedAtDoor = 1
         friendsRoomDoor}

        default{
        Write-Host "`nNot recognized. Please try again."
        friendsRoomDoor
        }
    }
}


function roomOrLiving{

Write-Host "`nBack in the hallway, you see the door to the room you woke up in."

Write-Host "`nWhat will you do?
1. Go back into the room
2. Go into the living room"

$choice = Read-Host "`nEnter your choice (1 or 2)"

switch ($choice)
{

    1
    {
        Write-Host "`nYou go back into your room. One look at the bed and you feel instantly tender. You muster all your strength to jump back up, and fall into a gentle sleep."
        gameOver
    }

    2
    {
        Write-Host "`nYou journey into the living room."
        livingRoom
    }

    default
    {
        Write-Host "`nNot recognized. Please try again."
        sleep 2
        roomOrLiving
    }
}
}

function livingRoom
{

    Write-Host "`nThe banging continues as you approach the kitchen area. You pinpoint the cause of the sound coming from a small room to your left."
    sleep 1
    Write-Host "`n*BANG*"
    sleep 1
    Write-Host "`n*BANG*"
    sleep 1
    Write-Host "`n*BANG*"
    sleep 1

    Write-Host "`nTo your right, light streams in from outside.`nWhat will you do?
    1. Investigate the small room
    2. Go unto the light"

    $choice = Read-Host  "`nEnter your choice (1 or 2)"

    switch ($choice)
{
    1
    {
       approach
    }
    2
    {
        Write-Host "`nYou investigate the door, but cannot open it."
        livingRoom
    }

    default
    {
        Write-Host "`nNot recognized. Please try again."
        livingRoom
    }
}
}

function approach{

        $choice = ''
        Write-Host "`nYou draw closer to the room"
        sleep 1
        Write-Host "`n*BANG*"
        sleep 1
        Write-Host "`n*BANG*"
        sleep 1
        Write-Host "`n*BANG*"
        sleep 1
        Write-Host "`n*BOOOOOM*"

        Write-Host "`nThe last bang is so loud, your 'Flight or Flight' instinct kicks in. Tail between your legs, you turn around and dart.`nWith your head scrambled, you can only think of escape. Where do you go?`n
        1. Back to the room
        2. Unto the light"

        $choice = Read-Host "`nEnter your choice (1 or 2)"

        switch($choice)
        {

            1{
                Write-Host "`nFearfully fast, you sprint back to the room, jump onto the bed and curl up tenderly. You can still hear the banging in the distance, so you close your eyes as tight as you can and go to sleep."
                gameOver
        
            }

            2{
            if($borkedAtDoor -eq 1){
                borked
             }
             else{
             Write-Host "`nYour mind cannot comprehend how to open the mighty door, and the loud banging continues. Nobody comes to help you, so you sprint back to the room, jump onto the bed and curl up tenderly. You can still hear the banging in the distance, so you close your eyes as tight as you can and go to sleep."
                gameOver
             }
             }
             default{
             Write-Host "`nNot recognized. Please try again."
             firstChoice
        }
}
}

function borked{

                Write-Host "`nYour mind cannot comprehend how to open the mighty door, and the loud banging continues. Just as all hope seems to be lost, you hear something... a voice... no... two voices."
                sleep 1
                Write-Host "`nYour friends jump into the living room, chanting your name over and over in consecutively higher tones. Suddenly, they're grabbing you, petting you, hugging you, pulling your chub... You forget all about the banging as your tail wags back and forth with impossible happiness."
                sleep 1
                Write-Host "`n '@#$ Borked at our door! #$#%@Outside!@#$%@Play?', you hear some words you recognize from the girl."
                sleep 1
                Write-Host "`n '!#$#%@Outside!@#$%@Play?', the boy repeats."
                sleep 1
                Write-Host "`nYou jump up at him, nipping at his arm with excitement. They open the mighty door, ushering you out. With a squeak, a thump and a ball flying through the air, you run outside, feeling the cool breeze and warm sun and total happiness all at once!"
                sleep 1
                goodEnding
}



function goodEnding{
Write-Host -ForegroundColor Yellow "`n The end! Thanks for playing! You got the good ending!"
sleep 2
Write-Host "`n~EPILOGUE~"
Write-Host "`n'What on earth is that banging?'"
sleep 2
Write-Host "`n'What the heck... somebody put like twenty shoes into the dryer! Why on earth would somebody do that...'"
sleep 2
Write-Host "`n'Who knows. Who knows..."
sleep 2
Write-Host "`nThe end!"

Read-Host "`nPress any key to exit"
}


function gameOver{
Write-Host -ForegroundColor Yellow "`nThe end! Thanks for playing! Try to get the good ending!"
Read-Host "`nPress any key to exit"
}


startGame